import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseUrlService {
  BASE_URL = 'http://localhost:8080';

  constructor() {
  }

  getBaseUrl(): string {
    return this.BASE_URL;
  }
}
