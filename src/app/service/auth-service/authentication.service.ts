import {Injectable} from '@angular/core';
import {BaseUrlService} from '../base-url.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private baseUrl: BaseUrlService,
              private http: HttpClient) {
  }

  registerUser(newUserDetails: JSON): any {
    return this.http.post(this.baseUrl.getBaseUrl() + '/auth-service/user/register', newUserDetails);

  }

  loginUser(): void {
  }

  generateToken(username: string, password: string): any {
    const formData = new FormData();
    formData.append('username', username);
    formData.append('password', password);
    formData.append('grant_type', 'password');
    const headers = new HttpHeaders({

      Authorization: `Basic ${btoa('mobile:pin')}`
    });
    return this.http.post(this.baseUrl.getBaseUrl() + '/auth-service/oauth/token', formData, {
      headers,
      withCredentials: true
    });
  }

  setAccessToken(accessToken): void {
    localStorage.setItem('access_token', accessToken);
  }

  setRefreshToken(refreshToken): void {
    localStorage.setItem('refresh_token', refreshToken);
  }

  getAccessToken(): string {
    return localStorage.getItem('access_token');
  }

  getRefreshToken(): string {
    return localStorage.getItem('refresh_token');
  }

  isLoggedIn(): boolean {
    const token = this.getAccessToken();
    if (token === undefined || token === null || token === '') {
      return false;
    } else {
      return true;
    }
  }

  logout(): any {
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('access_token');
    window.location.href = '/home';
  }
}
