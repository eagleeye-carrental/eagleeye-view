import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseUrlService} from '../../base-url.service';

@Injectable({
  providedIn: 'root'
})
export class HirePointService {

  constructor(private http: HttpClient,
              private baseUrl: BaseUrlService) {
  }

  hirePoints(): any {
    return this.http.get(`${this.baseUrl.getBaseUrl()}/vehicle/vehicle/hirepoint/`);
  }

  createHirePoint(value: any): any {
    return this.http.post(`${this.baseUrl.getBaseUrl()}/vehicle/vehicle/hirepoint/addnew`, value);
  }
}
