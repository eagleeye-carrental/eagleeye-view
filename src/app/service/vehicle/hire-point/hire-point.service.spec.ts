import { TestBed } from '@angular/core/testing';

import { HirePointService } from './hire-point.service';

describe('HirePointService', () => {
  let service: HirePointService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HirePointService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
