import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseUrlService} from '../../base-url.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManufacturerService {

  constructor(private http: HttpClient,
              private baseUrl: BaseUrlService) {
  }

  manufacturers(): any {
    return this.http.get(`${this.baseUrl.getBaseUrl()}/vehicle/vehicle/manufacturer/`);
  }

  createManufacturer(value: any): any {
    return this.http.post(`${this.baseUrl.getBaseUrl()}/vehicle/vehicle/manufacturer/addnew`, value);
  }
}
