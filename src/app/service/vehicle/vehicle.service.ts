import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseUrlService} from '../base-url.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private http: HttpClient,
              private baseUrl: BaseUrlService) {
  }


  getAllVehicle(): any {
    return this.http.get('http://localhost:8080/vehicle/vehicle/');
  }
}
