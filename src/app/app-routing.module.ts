import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';
import {HomeComponent} from './modules/home/home.component';


const routes: Routes = [
    {
      path: 'auth',
      loadChildren: () => import ('./modules/authentication/auth.module')
        .then(mod => mod.AuthModule)
    },
    {
      path: '', component: HomeComponent,
      canActivate: [AuthGuard],
      children: [{
        path: 'vehicle',
        loadChildren: () => import('./modules/vehicle/vehicle.module')
          .then(mod => mod.VehicleModule)
      }]
    }

  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
