import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../service/auth-service/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public authentication: AuthenticationService) { }

  ngOnInit(): void {
  }

}
