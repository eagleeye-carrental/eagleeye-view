import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../../service/auth-service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  errorMessage: string;
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthenticationService) {
    this.loginForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  ngOnInit(): void {
  }

  login(): any {
    this.errorMessage = null;
    this.checkUsernamePassword();
    const username = this.loginForm.get('username').value;
    const password = this.loginForm.get('password').value;
    if (username != null && password != null) {
      this.authService.generateToken(this.loginForm.get('username').value, this.loginForm.get('password').value).subscribe(
        result => {
          this.authService.setAccessToken(result.access_token);
          this.authService.setRefreshToken(result.refresh_token);
          window.location.href = '';
        },
        error => {
          this.errorMessage = error.error.error_description;
          console.log(this.errorMessage);
        }
      );
    }
  }

  private checkUsernamePassword(): any {
    const controls = this.loginForm.controls;
    Object.keys(controls)
      .forEach(prop => {
        const formcontrol = this.loginForm.get(prop);
        if (formcontrol.status === 'INVALID') {
          console.log(prop + ' required');
          formcontrol.setErrors({
            error: prop + ' required'
          });
        }
      });
  }
}
