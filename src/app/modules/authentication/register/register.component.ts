import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../../service/auth-service/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(private fb: FormBuilder,
              private authService: AuthenticationService) {
    this.registerForm = this.fb.group({
      firstName: [null, Validators.required],
      middleName: [null],
      lastName: [null, Validators.required],
      email: [null, Validators.required],
      mobileNumber: [null, Validators.required],
      password: [null, Validators.required],
      re_password: [null, Validators.required],
    });
  }

  ngOnInit(): void {
  }

  registerUser(): any {
    const controls = this.registerForm.controls;
    Object.keys(controls)
      .forEach(prop => {
        const formcontrol = this.registerForm.get(prop);
        if (formcontrol.status === 'INVALID') {
          console.log(prop + ' required');
          formcontrol.setErrors({
            error: prop + ' required'
          });
        }
      });
    const repassword = this.registerForm.get('re_password');
    const password = this.registerForm.get('password').value;
    if (password !== repassword.value) {
      repassword.setErrors({
        error: 'password mismatch'
      });
    }
    if (this.registerForm.status === 'VALID') {
      this.authService
        .registerUser(this.registerForm.value)
        .subscribe(
          result => {
            console.log(result);
          },
          error => {
            console.log(error);
          }
        );
    }
  }
}
