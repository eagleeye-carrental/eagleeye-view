import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HirePointComponent } from './hire-point.component';

describe('HirePointComponent', () => {
  let component: HirePointComponent;
  let fixture: ComponentFixture<HirePointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HirePointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HirePointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
