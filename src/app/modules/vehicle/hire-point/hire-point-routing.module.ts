import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HirePointComponent} from './hire-point.component';
import {AddHirePointComponent} from './add-hire-point/add-hire-point.component';
import {EditHirePointComponent} from './edit-hire-point/edit-hire-point.component';

const routes: Routes = [{
  path: '', component: HirePointComponent,
  children: [
    {path: 'add', component: AddHirePointComponent},
    {path: 'edit', component: EditHirePointComponent}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HirePointRoutingModule { }
