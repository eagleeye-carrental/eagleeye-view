import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { HirePointRoutingModule } from './hire-point-routing.module';
import { HirePointComponent } from './hire-point.component';
import { AddHirePointComponent } from './add-hire-point/add-hire-point.component';
import { EditHirePointComponent } from './edit-hire-point/edit-hire-point.component';
import {HirePointService} from '../../../service/vehicle/hire-point/hire-point.service';

import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FlexModule} from '@angular/flex-layout';
import {MatSnackBarModule} from '@angular/material/snack-bar';


@NgModule({
  declarations: [HirePointComponent, AddHirePointComponent, EditHirePointComponent],
  imports: [
    CommonModule,
    HirePointRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatToolbarModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    FlexModule
  ],
  providers: [HirePointService]
})
export class HirePointModule { }
