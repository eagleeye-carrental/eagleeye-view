import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditHirePointComponent } from './edit-hire-point.component';

describe('EditHirePointComponent', () => {
  let component: EditHirePointComponent;
  let fixture: ComponentFixture<EditHirePointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditHirePointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditHirePointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
