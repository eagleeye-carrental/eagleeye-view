import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddHirePointComponent } from './add-hire-point.component';

describe('AddHirePointComponent', () => {
  let component: AddHirePointComponent;
  let fixture: ComponentFixture<AddHirePointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddHirePointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddHirePointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
