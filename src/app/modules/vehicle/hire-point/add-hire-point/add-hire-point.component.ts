import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HirePointService} from '../../../../service/vehicle/hire-point/hire-point.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-add-hire-point',
  templateUrl: './add-hire-point.component.html',
  styleUrls: ['./add-hire-point.component.css']
})
export class AddHirePointComponent implements OnInit {
  hirePoint: FormGroup;

  constructor(private fb: FormBuilder,
              private hirePointService: HirePointService,
              private diglogRef: MatDialogRef<AddHirePointComponent>) {
    this.hirePoint = this.fb.group({
      hirePointName: [null, Validators.required],
      address1: [null, Validators.required],
      address2: [null],
      country: [null, Validators.required],
      city: [null, Validators.required],
      postcode: [null, Validators.required],
      email: [null, Validators.required],
      mobileNumber: [null, Validators.required],
    });
  }

  ngOnInit(): void {
  }

  createHirePoint(): void {
    console.log(this.hirePoint.value);
    const controls = this.hirePoint.controls;
    Object.keys(controls)
      .forEach(prop => {
        const formcontrol = this.hirePoint.get(prop);
        if (formcontrol.status === 'INVALID') {
          console.log(prop + ' required');
          formcontrol.setErrors({
            error: prop + ' required'
          });
        }
      });
    if (this.hirePoint.status === 'VALID') {
      this.hirePointService.createHirePoint(this.hirePoint.value)
        .subscribe(
          result => {
            this.diglogRef.close(result);
          },
          error => {
            console.log(error);
            const validationErrors = error.error.details;
            Object.keys(validationErrors)
              .forEach(prop => {
                const formControl = this.hirePoint.get(prop);
                if (formControl && validationErrors[prop] != null) {
                  formControl.setErrors({
                    error: validationErrors[prop]
                  });
                }
              });
          }
        );
    }
  }

  closeModule(): void {
    if (this.hirePoint.touched) {
      if (confirm('Are you sure, you don`t want to add new hire point?')) {
        this.diglogRef.close();
      }
    }
    this.diglogRef.close();
  }
}
