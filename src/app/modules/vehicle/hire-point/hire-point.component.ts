import {Component, OnInit, ViewChild} from '@angular/core';
import {HirePointService} from '../../../service/vehicle/hire-point/hire-point.service';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import {AddHirePointComponent} from './add-hire-point/add-hire-point.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-hire-point',
  templateUrl: './hire-point.component.html',
  styleUrls: ['./hire-point.component.css']
})
export class HirePointComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  hirepoint: MatTableDataSource<any>;
  details: any;
  searchkey: string;
  columns: string[] = ['hirePointName', 'address1', 'country', 'city',
    'postcode', 'email', 'mobileNumber', 'action'];

  constructor(private hirePointService: HirePointService,
              private matDialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.hirePointService.hirePoints().subscribe(
      result => {
        this.details = result.details;
        this.hirepoint = new MatTableDataSource(this.details);
        this.hirepoint.sort = this.sort;
        this.hirepoint.paginator = this.paginator;
      },
      error => {
        console.log(error);
      }
    );
  }

  applyFilter(): any {
    this.hirepoint.filter = this.searchkey.trim().toLowerCase();
  }

  onSearchClear(): any {
    this.searchkey = '';
    this.applyFilter();
  }

  openDialog(): void {
    const dialog = this.matDialog.open(AddHirePointComponent, {
      width: '30%',
      disableClose: true
    });

    dialog.afterClosed().subscribe(result => {
      if (result != null) {
        console.log(result);
        this.details.unshift({
          hirePointName: result.details.hirePointName,
          address1: result.details.address1,
          country: result.details.country,
          city: result.details.city,
          postcode: result.details.postcode,
          email: result.details.email,
          mobileNumber: result.details.mobileNumber,
          id: result.details.id
        });
        this.hirepoint = new MatTableDataSource(this.details);
        this.hirepoint.sort = this.sort;
        this.hirepoint.paginator = this.paginator;
        this.snackBar.open(`Hire Point Created`, '',
          {
            duration: 3500
          });
      }

    });
  }

  editHirePoint(id): void {
    console.log('hirepoint id', id);
  }
}
