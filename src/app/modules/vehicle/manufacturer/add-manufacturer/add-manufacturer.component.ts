import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {ManufacturerService} from '../../../../service/vehicle/manufacturer/manufacturer.service';

@Component({
  selector: 'app-add-manufacturer',
  templateUrl: './add-manufacturer.component.html',
  styleUrls: ['./add-manufacturer.component.css']
})
export class AddManufacturerComponent implements OnInit {
  manufacturer: FormGroup;

  constructor(private fb: FormBuilder,
              private manufacturerService: ManufacturerService,
              private dialogRef: MatDialogRef<AddManufacturerComponent>) {
    this.manufacturer = this.fb.group({
      manufacturerName: [null, Validators.required]
    });
  }

  ngOnInit(): void {
  }

  closeModule(): void {
    if (this.manufacturer.touched) {
      if (confirm('Are you sure, you don`t want to add new Manufacturer?')) {
        this.dialogRef.close();
      }
    }
    this.dialogRef.close();
  }

  createManufacturer(): any {
    const controls = this.manufacturer.controls;
    Object.keys(controls)
      .forEach(prop => {
        const formcontrol = this.manufacturer.get(prop);
        if (formcontrol.status === 'INVALID') {
          formcontrol.setErrors({
            error: prop + ' required'
          });
        }
      });
    if (this.manufacturer.status === 'VALID') {
      this.manufacturerService.createManufacturer(this.manufacturer.value)
        .subscribe(
          result => {
            this.dialogRef.close(result);
          },
          error => {
            console.log(error);
            const validationErrors = error.error.details;
            Object.keys(validationErrors)
              .forEach(prop => {
                const formControl = this.manufacturer.get(prop);
                if (formControl && validationErrors[prop] != null) {
                  formControl.setErrors({
                    error: validationErrors[prop]
                  });
                }
              });
          }
        );
    }
  }
}
