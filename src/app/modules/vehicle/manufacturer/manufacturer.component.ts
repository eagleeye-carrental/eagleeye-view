import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {ManufacturerService} from '../../../service/vehicle/manufacturer/manufacturer.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AddHirePointComponent} from '../hire-point/add-hire-point/add-hire-point.component';
import {AddManufacturerComponent} from './add-manufacturer/add-manufacturer.component';

@Component({
  selector: 'app-manufacturer',
  templateUrl: './manufacturer.component.html',
  styleUrls: ['./manufacturer.component.css']
})
export class ManufacturerComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  manufacturer: MatTableDataSource<any>;
  details: any;
  searchkey: string;
  columns: string[] = ['manufacturerName', 'action'];

  constructor(private manufacturerService: ManufacturerService,
              private matDialog: MatDialog,
              private snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    this.manufacturerService.manufacturers()
      .subscribe(
        result => {
          console.log(result.details);
          this.details = result.details;
          this.manufacturer = new MatTableDataSource(this.details);
          this.manufacturer.paginator = this.paginator;
          this.manufacturer.sort = this.sort;
        },
        error => {
          console.log(error);
        }
      );
  }

  editManufacturer(id): void {
    console.log(id);
  }

  openDialog(): void {
    const dialog = this.matDialog.open(AddManufacturerComponent, {
      width: '30%',
      disableClose: true
    });

    dialog.afterClosed().subscribe(result => {
      if (result != null) {
        console.log(result);
        this.details.unshift({
          manufacturerName: result.details.manufacturerName,
          id: result.details.id
        });
        this.manufacturer = new MatTableDataSource(this.details);
        this.manufacturer.sort = this.sort;
        this.manufacturer.paginator = this.paginator;
        this.snackBar.open(`Manufacturer Created`, '',
          {
            duration: 3500
          });
      }

    });
  }

  applyFilter(): void {
    this.manufacturer.filter = this.searchkey.trim().toLowerCase();
  }

  onSearchClear(): void {
    this.searchkey = '';
    this.applyFilter();
  }
}
