import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {VehicleComponent} from './vehicle.component';
import {AddVehicleComponent} from './add-vehicle/add-vehicle.component';
import {ListVehicleComponent} from './list-vehicle/list-vehicle.component';

const routes: Routes = [
  {
    path: '', component: VehicleComponent,
    children: [
      {path: 'add', component: AddVehicleComponent},
      {path: 'list', component: ListVehicleComponent},
      {
        path: 'hire-point', loadChildren: () => import('./hire-point/hire-point.module')
          .then(mod => mod.HirePointModule)
      },
      {
        path: 'manufacturer', loadChildren: () => import('./manufacturer/manufacturer.module')
          .then(mod => mod.ManufacturerModule)
      },
      {
        path: 'model', loadChildren: () => import('./model/model.module')
          .then(mod => mod.ModelModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleRoutingModule {
}
